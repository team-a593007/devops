#!/bin/bash

## Update and upgrade the system

sudo apt-get update && sudo sudo apt-get upgrade -y

## Install Apache

sudo apt-get install apache2 -y

## Install gitlab-runner

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

sudo apt-get install gitlab-runner

## Change the owner of the /var/www/html directory to gitlab-runner

sudo chown gitlab-runner /var/www/html -R

## Register runner add the token from gitlab, for that you need to go to the project -> settings -> CI/CD -> Runners -> Expand the runners section and copy the token
## Uncomment the following line and add the token

# sudo gitlab-runner register --url https://gitlab.com/